<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('demo')->group(function () {
    Route::post('/make-attempt-auth', 'Api\AuthController@makeAttempt');
    Route::post('/add-new-vehicle', 'Api\VehiclesController@addNewVehicle');
    Route::get('/get-vehicles', 'Api\VehiclesController@getCollection');
});