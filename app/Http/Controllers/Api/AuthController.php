<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Auth;
use Session;

class AuthController extends Controller
{
    public function makeAttempt(Request $request){
        $request = $request->json()->all();
        $result = false;
        $message = '';
        $payload = [];

        $user = Validator::make($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if($user->fails()){
            return response()->json([
                'result' => $result,
                'message'  => $user->errors(),
                'payload' => $payload,
                'status'   => 403
            ]);
        }else{
            $request = (object)$request;
            if (Auth::attempt(['email' => $request->email, 'password'=> $request->password])) {
                $result = true;
                $payload = [
                    'id' => Auth::user()->id,
                    'name' => Auth::user()->name,
                    'email' => Auth::user()->email,
                ];
            }else{
                $message = [
                    'user'=>'The credentials are not valid'
                ];
            }
            
            return response()->json([
                'result' => $result,
                'message'  => $message,
                'payload' => $payload,
                'status'   => 200
            ]);
        }
    }
}
