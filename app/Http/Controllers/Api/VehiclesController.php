<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Vehicle;
use Session;

class VehiclesController extends Controller
{
    public function addNewVehicle(Request $request){
        $request = $request->json()->all();
        $result = false;
        $message = '';
        $payload = [];

        // return Auth::user();

        // if(session()->has('payload')){
        //     return response()->json([
        //         'result' => $result,
        //         'message'  => [
        //             'auth'=>'The session time expire, please login again'
        //         ],
        //         'payload' => $payload,
        //         'status'   => 401
        //     ]);
        // }

        $vehicle = Validator::make($request, [
            'id' => 'required|integer',
            'type' => 'required|string',
            'name' => 'required|string',
            'color' => 'required|string',
            'wheels' => 'required|integer',
            'hp' => 'required|integer',
            'qty' => 'required|integer',
        ]);

        if($vehicle->fails()){
            return response()->json([
                'result' => $result,
                'message'  => $vehicle->errors(),
                'payload' => $payload,
                'status'   => 403
            ]);
        }else{
            $request = (object)$request;

            $newVehicle = new Vehicle();
            $newVehicle->type = $request->type;
            $newVehicle->name = $request->name;
            $newVehicle->color = $request->color;
            $newVehicle->wheels = $request->wheels;
            $newVehicle->hp = $request->hp;
            $newVehicle->qty = $request->qty;
            $newVehicle->save();

            $result = true;
            $payload = [
                'id' => $newVehicle->id,
                'type' => $newVehicle->type,
                'name' => $newVehicle->name,
                'color' => $newVehicle->color,
                'wheels' => $newVehicle->wheels,
                'hp' => $newVehicle->hp,
                'qty' => $newVehicle->qty,
            ];
            return response()->json([
                'result' => $result,
                'message'  => $message,
                'payload' => $payload,
                'status'   => 200
            ]);
        }
    }

    public function getCollection(Request $request){
        return response()->json([
            'result' => true,
            'message'  => '',
            'payload' => Vehicle::select('id', 'type', 'name', 'color', 'wheels', 'hp', 'qty')->get(),
            'status'   => 200
        ]);
    }
}
